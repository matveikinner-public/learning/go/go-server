package data

import (
	"fmt"

	"github.com/go-xorm/xorm"
	_ "github.com/lib/pq"
)

type User struct {
	Id       int64
	Email    string
	Password string `json:"-"`
}

func InitEngine() (*xorm.Engine, error) {
	dbHost := "127.0.0.1"
	dbPort := 5432
	dbUser := "postgres"
	dbPass := "secret"
	dbName := "postgres"

	dataSourceName := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", dbHost, dbPort, dbUser, dbPass, dbName)

	engine, err := xorm.NewEngine("postgres", dataSourceName)
	if err != nil {
		return nil, err
	}

	// Ping server to see if its alive
	if err := engine.Ping(); err != nil {
		return nil, err
	}

	fmt.Println("engine.Ping()", err)

	// Sync the database
	// TODO: Check what this actually does
	if err := engine.Sync(new(User)); err != nil {
		return nil, err
	}

	return engine, nil
}
