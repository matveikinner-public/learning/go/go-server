package main

import (
	"log"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt/v4"
	"gitlab.com/matveikinner-public/learning/go/go-server/data"
	"golang.org/x/crypto/bcrypt"
)

type Book struct {
	Name   string
	Author string
}

type Credentials struct {
	Email    string `validate:"required,email,min=6,max=32"`
	Password string `validate:"required,min=3,max=32"`
}

func getBook(ctx *fiber.Ctx) error {
	harryPotter := Book{"The Pholosopher Stone", "Agatha Christie"}

	return ctx.Status(fiber.StatusOK).JSON(harryPotter)
}

func main() {
	app := fiber.New()

	engine, err := data.InitEngine()
	if err != nil {
		// Panic to stop the execution process
		panic(err)
	}

	app.Get("/", func(ctx *fiber.Ctx) error {
		return ctx.SendString("Hello, World!")
	})

	app.Get("/book", getBook)

	app.Post("/register", func(ctx *fiber.Ctx) error {
		// Attempt to parse req to struct
		req := new(Credentials)

		if err := ctx.BodyParser(req); err != nil {
			return err
		}

		// There should be some validation logic here
		// See https://docs.gofiber.io/guide/validation
		// See https://github.com/go-playground/validator

		// Hash user password before saving it to the database
		hashedPassword, err := bcrypt.GenerateFromPassword([]byte(req.Password), bcrypt.DefaultCost)
		if err != nil {
			return err
		}

		newUser := &data.User{
			Email:    req.Email,
			Password: string(hashedPassword),
		}

		// We don't care about the result, only about an error, as if there is an error there will be no result obviously
		_, err = engine.Insert(newUser)
		if err != nil {
			return err
		}

		token, exp, err := createJWTToken(newUser)
		if err != nil {
			return err
		}

		return ctx.JSON(fiber.Map{"token": token, "exp": exp, "user": newUser})
	})

	app.Post("/login", func(ctx *fiber.Ctx) error {
		// Attempt to parse req to struct
		req := new(Credentials)

		if err := ctx.BodyParser(req); err != nil {
			return err
		}

		user := new(data.User)
		has, err := engine.Where("email = ?", req.Email).Desc("id").Get(user)
		if err != nil {
			return err
		}

		if !has {
			return fiber.NewError(fiber.StatusBadRequest, "Invalid login credentials")
		}

		if err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(req.Password)); err != nil {
			return err
		}

		token, exp, err := createJWTToken(user)
		if err != nil {
			return err
		}

		return ctx.JSON(fiber.Map{"token": token, "exp": exp, "user": user})
	})

	log.Fatal(app.Listen(":3000"))
}

// JWT Logic

func createJWTToken(user *data.User) (string, int64, error) {
	exp := time.Now().Add(time.Minute * 30).Unix()
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"user":  user.Id,
		"email": user.Email,
		"nbf":   time.Date(2015, 10, 10, 12, 0, 0, 0, time.UTC).Unix(),
		"exp":   exp,
	})

	t, err := token.SignedString([]byte("secret"))
	if err != nil {
		return "", 0, err
	}

	return t, exp, nil
}
